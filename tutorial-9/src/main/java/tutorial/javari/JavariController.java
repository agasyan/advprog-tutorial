package tutorial.javari;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import tutorial.javari.animal.Animal;

import java.util.List;

@RestController
public class JavariController {
    // TODO Implement me!

    @RequestMapping(value = "/javari" , method = RequestMethod.GET)
    public List<Animal> ListAnimal(){
        if(JavariCRUD.getRecords().isEmpty()) {
            throw new RecordsNull("WARNING! Records is Empty");
        }
        return JavariCRUD.getRecords();
    }

    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    private class RecordsNull extends RuntimeException{
        private RecordsNull(String msg){
            super(msg);
        }
    }

    @RequestMapping(value = "/javari", method = RequestMethod.POST)
    public String addAnimals(@RequestBody String animalString) {
        JavariCRUD.addAnimal(animalString);
        return "Animal Added to Records";
    }


    @RequestMapping(value = "/javari/{id}" , method = RequestMethod.GET)
    public Animal getAnimalById(@PathVariable ("id") int id) {
        if (JavariCRUD.getAnimalId(id) == null) throw new RecordsNull("WARNING!, Animal With "+ id +" NOT EXIST");
        return JavariCRUD.getAnimalId(id);
    }

    @RequestMapping(value = "/javari/{id}", method = RequestMethod.DELETE)
    public Animal removeAnimalById(@PathVariable("id") int id) {
        return JavariCRUD.removeAnimalFromRecords(id);
    }

}
