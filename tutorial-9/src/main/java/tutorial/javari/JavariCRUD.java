package tutorial.javari;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.util.List;
import java.util.ArrayList;

public class JavariCRUD {
    private static final String CsvPath = "tutorial-9/src/main/java/tutorial/javari/AnimalRecords.csv";
    private static final Path animalRecords = Paths.get("", CsvPath);

    public static List<Animal> getRecords() {
        List<Animal> animals = new ArrayList<>();
        try {
            Files.lines(animalRecords).forEach(i -> animals.add(convertFromCSV(i)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return animals;
    }

    public static Animal getAnimalId(int id) {
        List<Animal> animals = new ArrayList<>();

        try {
            Files.lines(animalRecords).filter(i -> Integer.parseInt(i.split(",")[0]) == id)
                    .forEach(found -> animals.add(convertFromCSV(found)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!animals.isEmpty()) {
            return animals.get(0);
        } else return null;
    }

    private static Animal convertFromCSV(String filesLine) {
        String[] splitFiles = filesLine.split(",");
        int id = Integer.parseInt(splitFiles[0]);
        String type = splitFiles[1];
        String name = splitFiles[2];
        Gender gender = Gender.parseGender(splitFiles[3]);
        double length = Double.parseDouble(splitFiles[4]);
        double weight = Double.parseDouble(splitFiles[5]);
        Condition condition = Condition.parseCondition(splitFiles[6]);
        return new Animal(id, type, name, gender, length, weight, condition);
    }

    public static void addAnimal(String animalString) {
        Animal animal = null;
        try {
            animal = convertJSONtoAnimal(animalString);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String outCsv = convertAnimaltoCsv(animal);
        StringBuilder stringBuilder = new StringBuilder();

        try {
            Files.lines(animalRecords).forEach(i -> stringBuilder.append(i + "/n"));
            stringBuilder.append(outCsv);
            writeToRecords(stringBuilder.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void writeToRecords(String out) throws IOException {
        BufferedWriter toCSV = new BufferedWriter(new FileWriter(CsvPath));
        toCSV.write(out);
        toCSV.flush();
        toCSV.close();
    }

    private static Animal convertJSONtoAnimal(String animal) throws IOException {
        JsonNode jsonNode = new ObjectMapper().readTree(animal);
        int id = jsonNode.get("id").intValue();
        String type = jsonNode.get("type").textValue();
        String name = jsonNode.get("name").textValue();
        Gender gender = Gender.parseGender(jsonNode.get("gender").textValue());
        double length = jsonNode.get("length").doubleValue();
        double weight = jsonNode.get("weight").doubleValue();
        Condition condition = Condition.parseCondition(jsonNode.get("condition").textValue());
        return new Animal(id, type, name, gender, length, weight, condition);
    }

    private static String convertAnimaltoCsv(Animal animal) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(animal.getId());
        stringBuilder.append(",");
        stringBuilder.append(animal.getType());
        stringBuilder.append(",");
        stringBuilder.append(animal.getName());
        stringBuilder.append(",");
        stringBuilder.append(animal.getGender());
        stringBuilder.append(",");
        stringBuilder.append(animal.getLength());
        stringBuilder.append(",");
        stringBuilder.append(animal.getWeight());
        stringBuilder.append(",");
        stringBuilder.append(animal.getCondition());
        return stringBuilder.toString();
    }

    public static Animal removeAnimalFromRecords(int id){
        if(isAnimalExists(id)){
            Animal animal = getAnimalId(id);
            StringBuilder stringBuilder = new StringBuilder();
            try {
                Files.lines(animalRecords)
                        .filter(i -> Integer.parseInt(i.split(",")[0]) != id)
                        .forEach(i -> stringBuilder.append(i + "\n"));
                stringBuilder.setLength(stringBuilder.length()-1);
                writeToRecords(stringBuilder.toString());
            } catch (IOException e){
                e.printStackTrace();
            }
        }
        return null;
    }

    private static boolean isAnimalExists(int id) {
        try {
            return Files.lines(animalRecords)
                    .anyMatch(i -> Integer.parseInt(i.split(",")[0]) == id);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
}
